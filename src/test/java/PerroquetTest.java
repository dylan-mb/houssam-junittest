/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.junit_test.Perroquet;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author dylan
 */
public class PerroquetTest {
    Perroquet p;
    
    public PerroquetTest() {
        p = new Perroquet();        
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @DisplayName("Test Perroquet.perroquetParlant()")
    @Test
    void testPerroquetParlant() {
        assertEquals("Ceci est une phrase test", p.perroquetParlant("Ceci est une phrase test"));
    }
    
    @DisplayName("Test Perroquet.perroquetBonjour()")
    @Test
    void testPerroquetBonjour() {
        assertEquals("Bonjour Dylan", p.perroquetBonjour("Dylan"));
    }
}
