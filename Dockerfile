FROM payara/micro:5.2021.10-jdk11

USER root

RUN apk add --no-cache fontconfig ttf-dejavu

COPY /target/Junit_Test-1.0-SNAPSHOT.jar /opt/payara/deployments/calculator.jar